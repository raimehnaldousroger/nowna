<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Mailer {

		function __construct () {
			$CI =& get_instance();

			$CI->load->library( 'email' );

			$config['protocol'] = 'smtp';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['mailtype'] = 'html';
			$config['wordwrap'] = TRUE;
			$config['smtp_host'] = 'in-v3.mailjet.com';
			$config['smtp_port'] = '587';
			// $config['smtp_user'] = '879932d434b09c2006e1889421bfee34';
			// $config['smtp_pass'] = '3d8247fc7b10eb6734dcd33bfec1ace1';
			$config['smtp_user'] = '6f77ff550cfd25b5eceac5437ddc75cc';
			$config['smtp_pass'] = '43e9173e27ba3f41d4548234fba70092';
			$config['smtp_crypto'] = 'tls ';

			$CI->email->initialize( $config );
		}

		public function send_mail ( $data ) {
			$CI =& get_instance();

			$CI->email->from( $data['from'], $data['fromName'] );
			$CI->email->to( $data['to'] );

			$CI->email->subject( $data['subject'] );
			$CI->email->message( $data['message'] );

			$CI->email->send();
		}
	}
