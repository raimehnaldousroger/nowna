<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . '/libraries/REST_Controller.php';

    class Article extends REST_Controller {

        public function __construct ()
        {
            header( 'Access-Control-Allow-Origin: *' );
            header( "Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE" );
            parent::__construct();
            date_default_timezone_set( 'Asia/Manila' );
            $this->load->model('article_model', 'article');
        }

        function article_list_get()
        {
            $user = $this->_getUser( ($this->input->get( 'token' )) );

            $data = $this->article->article_list();

            return $this->response( array(
                'status'   => TRUE,
                'response' => $data,
            ), REST_Controller::HTTP_OK );
        }
    }