<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require APPPATH . '/libraries/REST_Controller.php';

	class Site extends REST_Controller {

		function __construct () {

			header( 'Access-Control-Allow-Origin: *' );
			header( "Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE" );
			parent::__construct();
			date_default_timezone_set( 'Asia/Manila' );
		}

		function index_get () {
			// do not remove this
			show_404();
		}

		function login_admin_post ()
		{
			if ( $this->_validate( 'login' ) ) {
				$username = $this->input->post( 'username' );
				$password = $this->input->post( 'password' );

				$response = $this->customers->loginAdmin( $username, $password );

				if ( $response ){
					$member_found = $this->customers->get_by_attribute( 'customer_username', $username );

					if ( $member_found ) {
						unset( $member_found->customer_password );
						$this->response( array(
							'status'   => TRUE,
							'response' => $member_found,
							'token'    => $this->auth->get_token( $username )
						), REST_Controller::HTTP_OK );
					}
				}
			}
			$this->response( array(
				'status'  => FALSE,
				'message' => 'Incorrect Credentials'
			), REST_Controller::HTTP_BAD_REQUEST );
		}

		function login_user_post ()
		{
			if ( $this->_validate( 'login' ) ) {
				$username = $this->input->post( 'username' );
				$password = $this->input->post( 'password' );
				$response = $this->customers->loginUser( $username, $password );
				if ( $response ) {
					$member_found = $this->customers->get_by_attribute( 'customer_username', $username );
					if ( $member_found ) {
						unset( $member_found->customer_password );
						$this->response( array(
							'status'   => TRUE,
							'response' => $member_found,
							'token'    => $this->auth->get_token( $username )
						), REST_Controller::HTTP_OK );
					} else {
						$this->response( array(
							'status'  => FALSE,
							'message' => 'Your account is not active'
						), REST_Controller::HTTP_BAD_REQUEST );
					}
				}
			}
			$this->response( array(
				'status'  => FALSE,
				'message' => 'Incorrect Credentials'
			), REST_Controller::HTTP_BAD_REQUEST );
		}

		function login_admin_get ()
		{
			$user = $this->_getUser( ($this->input->get( 'token' )) );
			if ( $user ) {
				$this->response( array(
					'status'   => TRUE,
					'response' => $user
				), REST_Controller::HTTP_OK );
			}
		}

		function login_user_get ()
		{
			$user = $this->_getUser( ($this->input->get( 'token' )) );
			if ( $user ) {
				$this->response( array(
					'status'   => TRUE,
					'response' => $user
				), REST_Controller::HTTP_OK );
			}
		}

		function logout_post ()
		{
			if ( $this->_validate( 'logout' ) ) {
				$this->auth->delete_where( array( 'token' => $this->input->post( 'token' ) ) );
				$this->response( array(
					'status'   => TRUE,
					'response' => 'User successfully logs out.'
				), REST_Controller::HTTP_OK );
			}
			$this->response( array(
				'status'  => FALSE,
				'message' => 'Error occurred'
			), REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
		}

		function register_post ()
		{
			$data = array(
				"customer_username"        	=> $this->input->post( 'username' ),
				"customer_password"        	=> password_hash( $this->input->post( 'password' ), PASSWORD_BCRYPT ),
				"customer_name"      		=> $this->input->post( 'name' ),
				"customer_email"      		=> $this->input->post( 'email' ),
				"customer_date_registered" 	=> date( 'Y-m-d H:i:s' ),
				"customers_level"          	=> '3',
			);
			$new_customer = $this->customers->save( $data );
			if ( $new_customer ) {
				$this->response( array(
					'status'   => TRUE,
					'response' => $new_customer,
				), REST_Controller::HTTP_OK );
			}
			$this->response( array(
				'status'  => FALSE,
				'message' => 'Error occurred'
			), REST_Controller::HTTP_BAD_REQUEST );
		}

		function config_post ( $action = 'add' )
		{
			if ( $this->_validate( 'config' ) ) {
				$data = array(
					"config_name"  => $this->input->post( 'name' ),
					"config_value" => $this->input->post( 'value' )
				);

				if ( $action == 'add' ) {
					$new_config = $this->configs->save( $data );
				} else {
					$new_config = $this->configs->update( array(
						'config_name' => $this->input->post( 'name' )
					), array( 'config_value' => $this->input->post( 'value' ) ) );
				}
				if ( $new_config ) {
					$this->response( array(
						'status'   => TRUE,
						'response' => $new_config,
					), REST_Controller::HTTP_OK );
				}
			}
			$this->response( array(
				'status'  => FALSE,
				'message' => 'Error occurred'
			), REST_Controller::HTTP_BAD_REQUEST );
		}

		function config_get ( $name = FALSE )
		{
			$site_configs = ($name) ? $this->configs->get_by_attribute( 'config_name', $name ) : $this->configs->find_all();
			$this->response( array(
				'status'   => TRUE,
				'response' => ($site_configs) ? $site_configs : array(),
			), REST_Controller::HTTP_OK );
		}


		function _validate ( $action = "login" )
		{
			if ( $action == "login" ) {
				$this->form_validation->set_rules( 'username', 'username', 'strip_tags|trim|required' );
				$this->form_validation->set_rules( 'password', 'password', 'strip_tags|trim|required' );
			} else if ( $action == "config" ) {
				$this->form_validation->set_rules( 'name', 'name', 'strip_tags|trim|required' );
				$this->form_validation->set_rules( 'value', 'value', 'strip_tags|trim|required' );
			} else if ( $action == "logout" ) {
				$this->form_validation->set_rules( 'token', 'token', 'required' );
			} else if ( $action == "register_user" ) {
				$this->form_validation->set_rules( 'username', 'username', 'strip_tags|trim|required|is_unique[customers.customer_username]' );
				$this->form_validation->set_rules( 'password', 'password', 'strip_tags|trim|required' );
				$this->form_validation->set_rules( 'first_name', 'first name', 'required' );
				$this->form_validation->set_rules( 'last_name', 'last name', 'required' );
			}
			$this->form_validation->set_error_delimiters( '', '' );
			if ( $this->form_validation->run( $this ) == FALSE ) {
				$this->response( array(
					'status'  => FALSE,
					'message' => $this->form_validation->error_array()
				), REST_Controller::HTTP_BAD_REQUEST );
			} else {
				return TRUE;
			}
		}
	}
