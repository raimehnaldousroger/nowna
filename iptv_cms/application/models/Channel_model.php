<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Channel_model extends MY_Model {
        private $table = 'channels';

        function channel_list() {
            $this->db->from("$this->table u");

            $result = $this->db->select('u.*')
                               ->get();

            $data = $result->result_array();

            $result->free_result();

            return $data;
        }
    }