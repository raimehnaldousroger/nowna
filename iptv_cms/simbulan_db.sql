-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2020 at 06:20 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simbulan_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `token` varchar(100) NOT NULL,
  `customer_id` mediumint(8) UNSIGNED NOT NULL,
  `date_accessed` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `access_token`
--

INSERT INTO `access_token` (`id`, `token`, `customer_id`, `date_accessed`, `ip`) VALUES
(4, 'e25a6e2ef3052c920b7a28a781e76068', 1, '2020-07-03 10:37:32', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `note` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `note`) VALUES
(1, 'ABBRASIVE', ''),
(2, 'ADHESIVE', ''),
(3, 'asd', 'asd'),
(4, 'vbn', 'vbn');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(64) NOT NULL,
  `name` varchar(32) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `company_name` varchar(200) NOT NULL,
  `company_status` enum('Active','Deactive','Pending') NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE `configs` (
  `config_id` mediumint(8) UNSIGNED NOT NULL,
  `config_name` varchar(255) NOT NULL,
  `config_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`) VALUES
(1, 'PHP'),
(2, 'USD'),
(3, 'HKD'),
(4, 'RMD');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` mediumint(8) UNSIGNED NOT NULL,
  `customer_employee_no` varchar(50) DEFAULT NULL,
  `customer_name` varchar(100) NOT NULL,
  `customer_username` varchar(50) NOT NULL,
  `customer_password` varchar(100) NOT NULL,
  `customer_email` varchar(100) NOT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `customer_image_url` varchar(255) NOT NULL,
  `customer_date_registered` datetime NOT NULL,
  `customers_level` int(11) NOT NULL,
  `customer_verified` set('Active','Deactivated','Blocked') NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `customer_employee_no`, `customer_name`, `customer_username`, `customer_password`, `customer_email`, `customer_address`, `customer_image_url`, `customer_date_registered`, `customers_level`, `customer_verified`) VALUES
(1, '0001', 'Rai', 'raimehnroger', '$2y$10$DAeI4UzKdLvGARax5Oef2elRXOV9VduZjdm/AWJTfEbtM5VPjt2hW', 'raimehnroger@gmail.com', 'valenzuela city', 'default', '2019-07-19 07:27:50', 1, 'Active'),
(8, '123', 'gibo', 'gibo', '$2y$10$NsMwTpll7D/CxOCvvSIWnuKeSn0BvuvkLBNvp/SSW.cEnnE.SUaga', '', NULL, '', '2020-06-15 21:58:35', 3, 'Active'),
(11, '567', 'user', 'user', '$2y$10$.gXk0tokFhyOe8aeNAPHn.lCgWgyj4WVnbMt6NuXpfmYMpsO41KNG', 'rai@gmail.com', NULL, '', '2020-06-15 22:04:33', 3, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT 0,
  `is_private_key` tinyint(1) NOT NULL DEFAULT 0,
  `ip_addresses` text DEFAULT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `api_key` varchar(40) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `method` enum('get','post','options','put','patch','delete') NOT NULL,
  `params` text DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(3) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(18);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `unit_stock` varchar(50) NOT NULL,
  `unit_order` varchar(50) NOT NULL,
  `reorder_level` varchar(50) NOT NULL,
  `item_code` varchar(50) NOT NULL,
  `dimension` varchar(50) NOT NULL,
  `uom` varchar(50) NOT NULL,
  `note` varchar(300) NOT NULL,
  `image` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `supplier_id`, `category_id`, `unit_stock`, `unit_order`, `reorder_level`, `item_code`, `dimension`, `uom`, `note`, `image`) VALUES
(1, 'item 1', 1, 1, '123', '1', '', 'item1', '', '', 'item1', ''),
(2, 'asd', 1, 3, '123', '2', '', 'asd123', '', '', 'asd', ''),
(3, 'item 3', 4, 2, '123', '1', '', 'item3', '', '', 'item 3', '');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `project_name` varchar(200) NOT NULL,
  `material_budget` double NOT NULL,
  `completion_date` date NOT NULL,
  `status` set('Approve','Disapprove','Pending') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_name`, `material_budget`, `completion_date`, `status`) VALUES
(1, 'AGSH-GFA', 920000, '2019-03-18', 'Pending'),
(4, 'asd123', 123897567, '2020-09-10', 'Pending'),
(5, 'qwerty', 123, '2020-07-10', 'Approve'),
(6, 'zxc', 123, '2020-07-10', 'Approve');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` int(11) NOT NULL,
  `po_number` int(11) NOT NULL,
  `date_prepared` date NOT NULL,
  `delivery_pickup_date` date NOT NULL,
  `issued_by` varchar(150) NOT NULL,
  `terms_payment` varchar(50) NOT NULL,
  `request_payment_number` int(11) NOT NULL,
  `dr_number` int(11) NOT NULL,
  `less_1_percent_ewt` int(11) NOT NULL,
  `less_2_percent_ewt` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `ewt_id_1` int(11) NOT NULL,
  `vat_id_1` int(11) NOT NULL,
  `cur_id` int(11) NOT NULL,
  `installation_charge` int(11) NOT NULL,
  `delivery_charge` int(11) NOT NULL,
  `other_charge` int(11) NOT NULL,
  `net_amount` double NOT NULL,
  `gross_amount` int(11) NOT NULL,
  `ewt_id_2` int(11) NOT NULL,
  `vat_id_2` int(11) NOT NULL,
  `delivery_address` varchar(300) NOT NULL,
  `vat_status` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `status` varchar(150) NOT NULL,
  `supplier_id_1` int(11) NOT NULL,
  `supplier_id_2` int(11) NOT NULL,
  `remarks` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `po_number`, `date_prepared`, `delivery_pickup_date`, `issued_by`, `terms_payment`, `request_payment_number`, `dr_number`, `less_1_percent_ewt`, `less_2_percent_ewt`, `discount`, `ewt_id_1`, `vat_id_1`, `cur_id`, `installation_charge`, `delivery_charge`, `other_charge`, `net_amount`, `gross_amount`, `ewt_id_2`, `vat_id_2`, `delivery_address`, `vat_status`, `currency_id`, `status`, `supplier_id_1`, `supplier_id_2`, `remarks`) VALUES
(1, 1111, '2019-01-30', '2019-01-30', '11', '30 PDC', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8919.64285714286, 9000, 1, 0, 'PUROK 1 MAMERTA VILLAGE, SAMPALOC SAN RAFAEL BULACAN', 1, 3, '', 0, 9, '');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_request`
--

CREATE TABLE `purchase_request` (
  `id` int(11) NOT NULL,
  `pr_number` varchar(50) NOT NULL,
  `receive_date` date NOT NULL,
  `received_by` varchar(300) NOT NULL,
  `project_id` int(11) NOT NULL,
  `jo_number` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `po_qty` int(11) NOT NULL,
  `product_code` varchar(150) NOT NULL,
  `po_number_id` int(11) NOT NULL,
  `balance` varchar(50) NOT NULL,
  `date_required` date NOT NULL,
  `remarks` text NOT NULL,
  `status` set('Approved','Disapproved','Rejected','Pending') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_request`
--

INSERT INTO `purchase_request` (`id`, `pr_number`, `receive_date`, `received_by`, `project_id`, `jo_number`, `quantity`, `unit_id`, `po_qty`, `product_code`, `po_number_id`, `balance`, `date_required`, `remarks`, `status`) VALUES
(1, '123', '2020-07-03', 'asd', 1, '123', 0, 0, 0, '1', 0, '', '2020-07-10', 'asd', 'Approved'),
(2, '0933', '2020-07-03', 'qwerty', 5, '0933', 0, 0, 0, '2', 0, '', '2020-07-10', 'qwerty', 'Approved'),
(3, '0933', '2020-07-03', 'asd', 5, '123', 0, 0, 0, '3', 0, '', '2020-07-10', 'asd', 'Approved');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE `schedule` (
  `id` int(11) NOT NULL,
  `po_number_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `delivery_pickup_date` date NOT NULL,
  `term_of_payment` int(11) NOT NULL,
  `request_for_payment_number` varchar(200) NOT NULL,
  `net_amount` double NOT NULL,
  `date-of_payment` date NOT NULL,
  `project_id` int(11) NOT NULL,
  `remarks` set('Approved','Disapproved','Pending','Rejected') NOT NULL,
  `crew` int(11) NOT NULL,
  `driver` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `po_number_id`, `supplier_id`, `delivery_pickup_date`, `term_of_payment`, `request_for_payment_number`, `net_amount`, `date-of_payment`, `project_id`, `remarks`, `crew`, `driver`, `product_id`) VALUES
(1, 1, 1, '2019-01-31', 0, '', 8919.64285714286, '0000-00-00', 0, 'Pending', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `setting_id` mediumint(8) UNSIGNED NOT NULL,
  `setting_name` varchar(255) NOT NULL,
  `setting_description` text NOT NULL,
  `setting_default_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `contact_person` varchar(300) NOT NULL,
  `contact_person_title` varchar(100) NOT NULL,
  `address` varchar(300) NOT NULL,
  `city` varchar(50) NOT NULL,
  `region` double NOT NULL,
  `postal_code` double NOT NULL,
  `country` varchar(30) NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `fax_number` varchar(20) NOT NULL,
  `tin_number` varchar(30) NOT NULL,
  `terms_of_payment` varchar(100) NOT NULL,
  `credit_limit` varchar(100) NOT NULL,
  `business_ownership` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_number` varchar(20) DEFAULT NULL,
  `business_registration` varchar(100) NOT NULL,
  `note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `name`, `contact_person`, `contact_person_title`, `address`, `city`, `region`, `postal_code`, `country`, `phone_number`, `fax_number`, `tin_number`, `terms_of_payment`, `credit_limit`, `business_ownership`, `email`, `mobile_number`, `business_registration`, `note`) VALUES
(1, 'ALLGEMEINE-BAU-CHEMIE PHIL., INC.', 'JESSIE ROXAS', 'SALES', '10/F, ASIAN STAR BLDG., 2402 ASEAN DRIVE, FILINVEST C.C., MAKATI CITY', 'ALABANG, MUNTINLUPA CITY', 3, 1445, 'PHILIPPINES', '816-7203', '', '', '30 PDC', '', '', '', '0922-8294-243 / 0922', '', 'VATABLE'),
(2, 'asds', 'asd', 'vc', '899 Puso street Coloong 1 Valenzuela City', 'Valenzuela City', 2, 1445, 'Philippines', '09338698026', 'undefined', '123456', '3 years', '213', 'Xelure Information Technologies', 'raimehnroger@gmail.com', '09338698026', 'Xelure Information Technologies', 'asd'),
(3, 'qwe', 'qwe', 'qwe', 'qwe', 'Valenzuela City', 0, 1445, 'Philippines', '09338698026', '123', '123', '123', '123', 'Xelure Information Technologies', 'qwe@gmail.com', '09338698026', 'qwe123', 'asd'),
(4, 'zxc', 'zxc', 'zxc', 'Unit 1801, The Peak Tower, 107 L.P. Leviste St., Salcedo Village, Makati City', 'Makati CIty', 0, 1445, 'Philippines', '09338608026', '123', '231', '213', '123', 'Xelure Information Technologies', 'raimehnroger@gmail.com', '123', 'Xelure Information Technologies', 'asd');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_trail`
--

CREATE TABLE `tbl_trail` (
  `id` int(11) NOT NULL,
  `action` varchar(500) NOT NULL,
  `date` datetime NOT NULL,
  `whom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `name`) VALUES
(1, 'BOTTLE'),
(2, 'LITER');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `id` int(11) NOT NULL,
  `plate_no` varchar(20) NOT NULL,
  `vehicle_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`id`, `plate_no`, `vehicle_type`) VALUES
(1, 'RFZ-102', 'Elf Dropside');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_token`
--
ALTER TABLE `access_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `token` (`token`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `date_accessed` (`date_accessed`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`config_id`),
  ADD UNIQUE KEY `config_name` (`config_name`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `customer_username` (`customer_username`),
  ADD UNIQUE KEY `customer_email` (`customer_email`);

--
-- Indexes for table `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`),
  ADD KEY `keys_client_id_fk` (`client_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `logs_api_key_fk` (`api_key`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_request`
--
ALTER TABLE `purchase_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`setting_id`),
  ADD KEY `setting_name` (`setting_name`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_trail`
--
ALTER TABLE `tbl_trail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_token`
--
ALTER TABLE `access_token`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `configs`
--
ALTER TABLE `configs`
  MODIFY `config_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `purchase_request`
--
ALTER TABLE `purchase_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `setting_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_trail`
--
ALTER TABLE `tbl_trail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `keys`
--
ALTER TABLE `keys`
  ADD CONSTRAINT `keys_client_id_fk` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_api_key_fk` FOREIGN KEY (`api_key`) REFERENCES `keys` (`key`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
