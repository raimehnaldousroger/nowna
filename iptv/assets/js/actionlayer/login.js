$('#login').click((e) => {
    e.preventDefault();
    var reqData = {};
    if(($('#login_username').val() === "") || ($('#login_password').val() === "")) {
        toastr.error('Username/Password is required.');
    } else {
        reqData.username = $('#login_username').val();
        reqData.password = $('#login_password').val();

        var bodyFormData = new FormData();
        bodyFormData.set('username', reqData.username );
        bodyFormData.set('password', reqData.password );

        axios.post(endPoints.LOGIN, bodyFormData, CONFIG.HEADER)
          .then( (response) => {
            var TOKEN = JSON.stringify(response.data.token.access_token);

            LocalStorage.add(STORAGE_ITEM.TOKEN, TOKEN);
            $(location).attr('href', viewRoutes.PORTAL);
          })
          .catch( (error) => {
            toastr.error('Invalid Credentials');
          })
    }
});
