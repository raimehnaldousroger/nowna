var token = JSON.parse(LocalStorage.get(STORAGE_ITEM.TOKEN));
$( window ).on('load',() => {
    $('.main-preloader').remove();
});

var timestamp = '<?=time()?>';
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}
function updateTime(){
    var dt = new Date();
    var dd = String(dt.getDate()).padStart(2, '0');
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";

    var date = month[dt.getMonth()] + " " + dd + ", " + dt.getFullYear();
    var time = formatAMPM(new Date);
    $('#time').html(date + ' | ' + time);
    timestamp++;
}
$(function(){
    setInterval(updateTime, 1000);
    setInterval(formatAMPM, 1000);
});

if (LocalStorage.get(STORAGE_ITEM.TOKEN)) {
    axios.get(endPoints.LOGIN.concat('?token=').concat(token), CONFIG.HEADER)
         .then( (response)  => {
          // $(location).attr('href', viewRoutes.PORTAL);
         })
         .catch( (error)  => {
            $(location).attr('href', viewRoutes.HOME);
      	 })
}
else {
	$(location).attr('href', viewRoutes.HOME);
}