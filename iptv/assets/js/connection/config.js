
var server = {
    'CISERVICE'                   : 'http://localhost/simbulan/api',
    'CISITE'                      : 'http://localhost/simbulan/master',
}

var viewRoutes = {
    'HOME'               		  : server.CISITE + '/',
    'LOGIN'                       : server.CISITE + '/signin',
    'LOGOUT'                      : server.CISITE + '/logout',
    'REGISTER'                    : server.CISITE + '/registration',
};

var endPoints = {
    // login / logout
    'LOGIN'                       : server.CISERVICE + '/site/login_admin',
    'LOGOUT'                      : server.CISERVICE + '/site/logout',
    // user manager
    'USERS_LIST'                  : server.CISERVICE + '/customer/users',
    'ADDUSER'                     : server.CISERVICE + '/site/register',
};
/** LOCAL WEB STORAGE ITEMS **/
var STORAGE_ITEM = {
    'TOKEN'                       : 'Token',
    'LOGIN'                       : 'Login',
};

var CONFIG = {
    'HEADER'                      : {'headers': { "Authorization": "Basic " + btoa('webPortal' + ":" + 'VV313p0Rt@l'), "X-API-KEY" : "4cww48g0cggc4kgggsckg8wo4kk8k8wowwgooo44" }},
};
