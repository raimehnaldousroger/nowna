<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']            =       'public/public_page_c/login';
$route['404_override']                  =       '';
$route['translate_uri_dashes']          =       FALSE;

$route['dashboard']                    	=       'user/dashboard_c';

// User management
$route['users']                			=      	'user/user_c';
$route['add-user']                		=      	'user/user_c/add_user';
$route['profile']               		=      	'user/user_c/info';
$route['profile/(:num)']               	=      	'user/user_c/info/$1';

// ERP
$route['erp']                			=      	'user/erp_c';
$route['pr-po']                			=      	'user/prPo_c';
// pr
$route['po']                			=      	'user/po_c';
$route['po-info/(:num)']                =      	'user/po_c/info/$1';
// po
$route['pr']                			=      	'user/pr_c';
$route['pr-info/(:num)']                =      	'user/pr_c/info/$1';
// supplier
$route['supplier']                		=      	'user/supplier_c';
$route['supplier-info/(:num)']          =      	'user/supplier_c/info/$1';
// projects
$route['projects']            			=      	'user/project_c';
$route['project_info/(:num)']      		=      	'user/project_c/info/$1';
// product
$route['products']            			=      	'user/product_c';
$route['product-add']            		=      	'user/product_c/add';
$route['product-info/(:num)']      		=      	'user/product_c/info/$1';
// Logistics
$route['logistics']            			=      	'user/logistics_c';
$route['logistics-info/(:num)']      	=      	'user/logistics_c/info/$1';