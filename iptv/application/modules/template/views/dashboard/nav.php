<?php defined('BASEPATH') OR exit('No direct script access allowed');

	$directoryURI = $_SERVER['REQUEST_URI'];
	$path = parse_url($directoryURI, PHP_URL_PATH);
	$components = explode('/', $path);
	$first_part = $components[3];
?>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.8/dist/vue.js"></script>

<script>
    $(document).ready(function() {
        var token = JSON.parse(LocalStorage.get(STORAGE_ITEM.TOKEN));
        new Vue({
            data: {
              userInfo: []
            },
            mounted() {
                this.setInfo();
            },
            computed: {

            },
            methods: {
                setInfo: async function () {
                    await axios.get(endPoints.LOGIN.concat('?token='+token), CONFIG.HEADER)
                        .then( (response) => {
                            this.userInfo = response.data.response;
                        })
                        .catch( (error) => {
                            console.log(error);
                        });
                },
            }
        }).$mount('#userinfo')
    });
</script>

<!-- Navbar -->
<nav class="pr-0 pl-0 main-header navbar navbar-expand bg-white navbar-light border-bottom">
  	<div class="container-fluid pl-2 pr-2 pl-md-5 pr-md-5">
      <!-- Left navbar links -->
      	<ul class="navbar-nav">
	        <li class="nav-item mr-md-3">
	            <div class="__vertical_center">
	               <a class="nav-link pr-3 pl-0 " data-widget="pushmenu" href="#">
	                   <span class="icon-bar"></span>
	                   <span class="icon-bar"></span>
	                   <span class="icon-bar"></span>
	               </a>
	            </div>
	        </li>
      	</ul>
		<a href="<?= site_url('dashboard') ?>">
			<img src="<?= base_url('assets/images/logo.png') ?>" alt="logo" class="img-fluid header-logo">
		</a>
  	</div>
</nav>
<!-- /.navbar -->

<!-- Main Sidebar container-fluid -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Sidebar -->
  	<div class="sidebar p0">
      <!-- Sidebar user panel (optional) -->
  		<div class="pl50 pt-2 mt-3 pb30 mb-3 d-flex" style="border-bottom: 1px solid #2150fd;">
  			<div id="userinfo">
  				<div class="pull-left image">
  					<p class="fs-25 fw-300 c-white" v-text="userInfo.customer_name"></p>
                    <p class="fs-13 fw-300 c-white" id="time"></p>
                    <a href="<?= site_url('profile') ?>">My Profile</a>
  				</div>
  			</div>
  		</div>
	  	<!-- Sidebar Menu -->
	  	<nav class="mt-5 ml-4 mr-4">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="<?= site_url('dashboard') ?>" class="nav-link <?= ($first_part == 'dashboard') ? 'active' : '';?>">
                        <p class="fs-20 fw-300">
                            <i class="fa fa-tachometer fs-20" aria-hidden="true"></i> Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= site_url('users') ?>" class="nav-link <?= ($first_part == 'users' || $first_part == 'add-user') ? 'active' : ''?>">
                        <p class="fs-20 fw-300">
                            <i class="fa fa-users fs-20" aria-hidden="true"></i> User Management
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= site_url('erp') ?>" class="nav-link <?= ($first_part == 'erp' || $first_part == 'pr-po'|| $first_part == 'pr-po'|| $first_part == 'pr'|| $first_part == 'po'|| $first_part == 'logistics'|| $first_part == 'supplier'|| $first_part == 'projects'|| $first_part == 'materials') ? 'active' : ''?>">
                        <p class="fs-20 fw-300">
                            <i class="fa fa-archive fs-20" aria-hidden="true"></i> ERP <span id="requestCount" style=""></span>
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
    <!-- /.sidebar-menu -->
	</div>
  	<div class="mt-3 ml40 mr-4">
        <a class="c-white fs-24 logout-btn" href="#">
	        <span class="__vertical_center left">
	            <span class="fs-15 fw-400 c-white">Logout</span>
	        </span>
      	</a>
  	</div>
  <!-- /.sidebar -->

</aside>
