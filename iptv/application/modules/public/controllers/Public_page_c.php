<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Public_page_c extends MX_Controller {
	function __construct() 
	{
		parent::__construct();

		$this->load->module('template');
	}

	function login() 
	{
		$this->load->library(['form_validation']);
		$this->form_validation->CI =& $this;

		$data['title']			=	'Login';

		$this->load->view('public/login', $data);
	}
}