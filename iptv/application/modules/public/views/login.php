<!DOCTYPE html>
<html>
<head>
    <title>Simbulan Group</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="company" content="Simbulan Group of Companies">
	<meta name="developers" content="Raimehn Roger">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700" rel="stylesheet">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assets/css/helper.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">

</head>
<body>
	<div class="container_fluid" style="height: 100vh">
		<div class="container __vertical_center">
			<div class="row">
				<div class="col-md-12">
					<div class="login_form col-md-7 mx-auto">
						<center><img src="<?= base_url('assets/images/logo.png') ?>" class="img-fluid mb30 w-50" id="icon" alt="User Icon" /></center>
						<center>
							<form method="POST">
								<div class="col-md-10">
				                <div class="form-group m0">
								    <input type="text" class="form-control" id="login_username" placeholder="Username" id="email">
							  	</div>
								<div class="form-group">
								    <input type="password" class="form-control" id="login_password" placeholder="Password" id="pwd">
							    </div>
					                <center><input type="submit" class="fs-15 btn btn-primary c-white w-30" id="login" value="Login"></center>
								</div>
							</form>
						</center>
					</div>
				</div>
				<div class="col-md-12">
		          	<div class="footer">
				 		<center><p>&copy; <?= date('Y') ?> Simbulan Group of Companies, Inc . All Rights Reserved.</p></center>
		          	</div>
				</div>
			</div>
		</div>
	</div>

    <!-- toastr -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <script>
		$(document).ready(function(){
			LocalStorage.delete(STORAGE_ITEM.TOKEN);
			LocalStorage.delete(STORAGE_ITEM.LOGIN);
		});
    </script>

    <script src="<?= base_url('assets/js/helpers/util.js') ?>"></script>
    <script src="<?= base_url('assets/js/connection/config.js') ?>"></script>

    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="<?= base_url('assets/js/actionlayer/login.js') ?>"></script>
</body>
</html>
