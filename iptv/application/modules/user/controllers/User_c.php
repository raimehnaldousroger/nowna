<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_c extends MX_Controller {
    function __construct()
    {
        parent::__construct();

        $this->load->module('template');
    }

    function index()
    {
        $data['title']          =   'User Management';
        $data['content_view']   =   'user/user_management/users';

        $this->template->dashboard($data);
    }


    function add_user($id = null)
    {
        $this->load->library(['form_validation']);
        $this->form_validation->CI =& $this;
        $data['title']          =   'Add User';
        $data['content_view']   =   'user/user_management/add-user';

        $this->template->dashboard($data);
    }


    function info($id = null)
    {
        $this->load->library(['form_validation']);
        $this->form_validation->CI =& $this;
        $data['title']          =   'User Detail';
        $data['content_view']   =   'user/user_management/info';

        $this->template->dashboard($data);
    }
}
