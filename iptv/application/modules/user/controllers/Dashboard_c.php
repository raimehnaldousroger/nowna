<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_c extends MX_Controller {
    function __construct()
    {
        parent::__construct();

        $this->load->module('template');
    }

    function index()
    {
        $data['title']          =   'Dashboard';
        $data['content_view']   =   'user/dashboard/dashboard';

        $this->template->dashboard($data);
    }
}
